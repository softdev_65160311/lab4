/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

import java.util.Scanner;

/**
 *
 * @author adisa
 */
public class Game {
    private Player player1;
    private Player player2;
    private Table table;
    
    public Game(){
        this.player1 = new Player('X');
        this.player2 = new Player('O');
    }
    
    public void newGame(){
        this.table = new Table(player1, player2);
    }
    
    public void showWelcome(){
        System.out.println("Welcome to OX");
    }
    
    public void showTable(){
        char[][] t = table.getTable();
        for(int row=0; row<3; row++){
            for(int col=0; col<3; col++){
                System.out.print(t[row][col]+" ");
            }
            System.out.println();
        }
    }
    
    public void showTurn(){
        System.out.println(table.getCurrentPlayer().getSymbol() + " Turn");
    }
    
    public void inputRowCol(){
        Scanner kb = new Scanner(System.in);
        while(true){
            System.out.print("Please input row col : ");
            int row = kb.nextInt();
            int col = kb.nextInt();
            if(table.setRowCol(row, col)){
                break;
            }
        }
    }
    
    public void showInfo(){
        System.out.println(player1);
        System.out.println(player2);
    }
    
    public boolean isContinue(){
        Scanner kb = new Scanner(System.in);
        System.out.print("Do you want to continue? (y/n) : ");
        char isCon = kb.next().charAt(0);
        if(isCon=='y'){
            return true;
        }
        return false;
        
    }
    
    
    public void play(){
        showWelcome();
        newGame();
        while(true){
            showTable();
            showTurn();
            inputRowCol();
            if(table.checkWin()){
                showTable();
                System.out.println(table.getCurrentPlayer().getSymbol()+" Win!");
                showInfo();
                if(isContinue()){
                    newGame();
                    continue;
                }else{
                    break;
                }    
            }
            if(table.checkDraw()){
                showTable();
                System.out.println("Draw!");
                showInfo();
                if(isContinue()){
                    newGame();
                    continue;
                }else{
                    break;
                }    
            }
            table.switchPlayer();
        }
    }
}
    
