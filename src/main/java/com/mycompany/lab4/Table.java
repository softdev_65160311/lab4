/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.lab4;

/**
 *
 * @author adisa
 */
public class Table {
    private char[][] table = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player player1;
    private Player player2;
    private Player currentPlayer;
    private int row;
    private int col;
    private int turnCount;
    
    public Table(Player player1, Player player2){
        this.player1 = player1;
        this.player2 = player2;
        this.currentPlayer = player1;
    }
    
    public char[][] getTable(){
        return table;
    }
    
    public Player getCurrentPlayer(){
        return currentPlayer;
    }
    
    public boolean setRowCol(int row, int col){
        if(table[row-1][col-1]=='-'){
            table[row-1][col-1]=currentPlayer.getSymbol();
            this.row = row;
            this.col = col;
            turnCount++;
            return true;
        }
        return false;
    }
    
    public void switchPlayer(){
        if(currentPlayer==player1){
            currentPlayer=player2;
        }else{
            currentPlayer=player1;
        }
    }
    
    public boolean checkWin(){
        if(checkRow()||checkCol()||checkDiagonal()){
            saveWin();
            return true;
        }
        return false;
    }
    
    private boolean checkRow(){
        for(int col=0; col<3; col++){
            if(table[row-1][col]!=currentPlayer.getSymbol()){
                return false;
            }
        }
        return true;
    }
    
    private boolean checkCol(){
        for(int row=0; row<3; row++){
            if(table[row][col-1]!=currentPlayer.getSymbol()){
                return false;
            }
        }
        return true;
    }
    
    private boolean checkDiagonal(){
        if(table[0][0]==currentPlayer.getSymbol()&&table[1][1]==currentPlayer.getSymbol()&&table[2][2]==currentPlayer.getSymbol()){
            return true;
        }
        if(table[0][2]==currentPlayer.getSymbol()&&table[1][1]==currentPlayer.getSymbol()&&table[2][0]==currentPlayer.getSymbol()){
            return true;
        }
        return false;
    }
    
    public boolean checkDraw(){
        if(turnCount==9){
            saveDraw();
            return true;
        }
        return false;
    }
    
    public void saveWin(){
        if(player1 == getCurrentPlayer()){
            player1.win();
            player2.lose();
        }else{
            player2.win();
            player1.lose();
        }
    }
    
    public void saveDraw(){
        player1.draw();
        player2.draw();
    }
    
}

